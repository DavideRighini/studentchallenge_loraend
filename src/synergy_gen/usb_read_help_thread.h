/* generated thread header file - do not edit */
#ifndef USB_READ_HELP_THREAD_H_
#define USB_READ_HELP_THREAD_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
void usb_read_help_thread_entry(void);
#include "sf_el_ux_comms.h"
#include "sf_comms_api.h"
#ifdef __cplusplus
extern "C" {
#endif
extern const sf_comms_instance_t g_sf_comms0;
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* USB_READ_HELP_THREAD_H_ */
