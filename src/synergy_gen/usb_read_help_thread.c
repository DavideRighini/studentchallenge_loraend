/* generated thread source file - do not edit */
#include "usb_read_help_thread.h"

TX_THREAD usb_read_help_thread;
void usb_read_help_thread_create(void);
static void usb_read_help_thread_func(ULONG thread_input);
/** Alignment requires using pragma for IAR. GCC is done through attribute. */
#if defined(__ICCARM__)
#pragma data_alignment = BSP_STACK_ALIGNMENT
#endif
static uint8_t usb_read_help_thread_stack[1024] BSP_PLACE_IN_SECTION(".stack.usb_read_help_thread") BSP_ALIGN_VARIABLE(BSP_STACK_ALIGNMENT);
sf_comms_ctrl_t g_sf_comms0_ctrl;
sf_el_ux_comms_on_comms_ctrl_t g_sf_comms0_ctrl_extend;
sf_el_ux_comms_on_comms_cfg_t g_sf_comms0_cfg_extend = { .p_ctrl =
		&g_sf_comms0_ctrl_extend, };
const sf_comms_cfg_t g_sf_comms0_cfg = { .p_extend = &g_sf_comms0_cfg_extend };
/* Instance structure to use this module. */
const sf_comms_instance_t g_sf_comms0 = { .p_ctrl = &g_sf_comms0_ctrl, .p_cfg =
		&g_sf_comms0_cfg, .p_api = &g_sf_el_ux_comms_on_sf_comms };
void usb_read_help_thread_create(void) {
	/* Initialize each kernel object. */

	tx_thread_create(&usb_read_help_thread, (CHAR *) "USB Read helper Thread",
			usb_read_help_thread_func, (ULONG) NULL,
			&usb_read_help_thread_stack, 1024, 5, 5, 1, TX_DONT_START);
}

static void usb_read_help_thread_func(ULONG thread_input) {
	/* Not currently using thread_input. */
	SSP_PARAMETER_NOT_USED(thread_input);

	/* Initialize each module instance. */
	ssp_err_t ssp_err_g_sf_comms0;
	ssp_err_g_sf_comms0 = g_sf_comms0.p_api->open(g_sf_comms0.p_ctrl,
			g_sf_comms0.p_cfg);
	if (SSP_SUCCESS != ssp_err_g_sf_comms0) {
		while (1)
			;
	}

	/* Enter user code for this thread. */
	usb_read_help_thread_entry();
}
