#include "main_thread.h"
#include "usb_read_help_thread.h"
#include "message_size.h"

extern TX_THREAD usb_read_help_thread;

static volatile bool transmitComplete = false;
static volatile uint16_t UART_charReceived = 0;
static volatile uint8_t UART_dest[256] = {0};

static volatile bool UART_Receive = false;
static volatile uint16_t UART_Size = 0;

void main_thread_entry(void);

/* Main Thread entry function */
void main_thread_entry(void)
{
    /* TODO: add your own code here */
    UINT status;
    ssp_err_t open_result, write_result;
    volatile uint8_t USB_charReceived = 0;
    uint8_t USB_dest[256] = {0};

    /*Open UART*/
    open_result = g_uart0.p_api->open(g_uart0.p_ctrl, g_uart0.p_cfg);
    if (SSP_SUCCESS != open_result)
    {
        while(1); /*Could not open UART*/
    }

    /*Infinite loop*/
    while (1)
    {
        /* Now UX comms framework is open, resume the helper thread */
        tx_thread_resume(&usb_read_help_thread);

        status = tx_queue_receive(&g_usb_read_queue, USB_dest+USB_charReceived, TX_NO_WAIT);
        if (TX_SUCCESS == status)
        {
            /* Echo the character back */
        	write_result = g_sf_comms0.p_api->write(g_sf_comms0.p_ctrl, USB_dest+USB_charReceived, QUEUE_MESSAGE_SIZE/(sizeof(UINT)), TX_NO_WAIT);

            if(USB_dest[USB_charReceived] =='\r')
            {/*A carriage return is detected*/

            	/* Echo the carriage return back */
                g_sf_comms0.p_api->write(g_sf_comms0.p_ctrl, "\n", 1, TX_NO_WAIT);

                if(USB_charReceived !=0)
                {
					/*Send message size on UART*/
					g_uart0.p_api->write(g_uart0.p_ctrl, &USB_charReceived, 1);
					while(!transmitComplete);/*Wait until end of transfer*/
					transmitComplete = false;

					/*Send message on UART*/
					g_uart0.p_api->write(g_uart0.p_ctrl, USB_dest, USB_charReceived);
					while(!transmitComplete);/*Wait until end of transfer*/
					transmitComplete = false;

//					write_result = g_sf_comms0.p_api->write(g_sf_comms0.p_ctrl,
//					                						"ok",
//															2,
//															TX_NO_WAIT);
//					write_result = g_sf_comms0.p_api->write(g_sf_comms0.p_ctrl,
//															USB_dest,
//															USB_charReceived,
//															TX_NO_WAIT);
//					write_result = g_sf_comms0.p_api->write(g_sf_comms0.p_ctrl,
//															"\n\r",
//															2,
//															TX_NO_WAIT);
					USB_charReceived = 0;
                }
                else
                {
                	write_result = g_sf_comms0.p_api->write(g_sf_comms0.p_ctrl,
                											"Carriage return not sent\r\n",
															26,
															TX_NO_WAIT);
                }

            }
            else
            {/*The message is not finished*/
                if(USB_charReceived<255)
                {
                	USB_charReceived++;
                }
            }

        }

        if(UART_Receive){

        	//write_result = g_sf_comms0.p_api->write(g_sf_comms0.p_ctrl, "Received message : ", 19, TX_NO_WAIT);

        	write_result = g_sf_comms0.p_api->write(g_sf_comms0.p_ctrl, UART_dest, UART_Size, TX_NO_WAIT);

        	//write_result = g_sf_comms0.p_api->write(g_sf_comms0.p_ctrl, "\r\n", 2, TX_NO_WAIT);

        	UART_Receive =false;
        }

        else
        {
            tx_thread_sleep(1); /* We didn't receive anything */
        }
    }
}


/* UART Callback function
 * Set Transmit Done flag when UART transmission is complete.
 * Manage received UART bytes.
*/
void user_uart_callback(uart_callback_args_t *p_args)
{
    static bool firstChar = true;
    if (p_args->event == UART_EVENT_TX_COMPLETE)/*Transmit complete event*/
    {
        // Set Transmit Complete Flag
        transmitComplete = true;
    }
    if(p_args->event == UART_EVENT_RX_CHAR)/*Received char event*/
    {
        if(firstChar)
        {
        	/* First char is UART message size */
            UART_Size = p_args->data;
            firstChar = false;
        }
        else
        {
            if(UART_Size != 0)
            {
            	/* Next bytes are the payload*/
                UART_dest[UART_charReceived] = p_args->data;
                UART_charReceived++;
                if(UART_Size == UART_charReceived)
                {
                	/*If message if complete*/
                    firstChar = true;
                    UART_charReceived = 0;
                    UART_Receive = true;/*Set UART receive flag to true*/
                }
            }
            else
            {
                firstChar = true;
            }
        }
    }

}
