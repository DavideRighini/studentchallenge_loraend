/*
 * message_size.h
 *
 *  Created on: 4 Oct 2016
 *      Author: BakerJ
 */

#ifndef MESSAGE_SIZE_H_
#define MESSAGE_SIZE_H_


#define QUEUE_MESSAGE_SIZE 4 /* Size of queue messages in bytes */

#endif /* MESSAGE_SIZE_H_ */
