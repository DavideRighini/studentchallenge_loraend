#include "usb_read_help_thread.h"
#include "main_thread.h"
#include "message_size.h"

void usb_read_help_thread_entry(void);

uint8_t g_read_message[QUEUE_MESSAGE_SIZE];

/* USB Read helper Thread entry function */
void usb_read_help_thread_entry(void)
{
    ssp_err_t err;
    UINT status;

    /* TODO: add your own code here */
    while (1)
    {
        /* The timeout parameter is the device enumeration timeout, not read character timeout */
        err = g_sf_comms0.p_api->read(g_sf_comms0.p_ctrl, g_read_message ,1 ,TX_NO_WAIT);
        if (SSP_SUCCESS == err)
        {
            status = tx_queue_send(&g_usb_read_queue, g_read_message, TX_WAIT_FOREVER);
            if (TX_SUCCESS != status)
            {
                ;
            }
        }
        else
        {
            ; /* Device enumeration timeout could have occurred */
        }

    }
}
